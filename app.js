
// ------------------------------------------------------------------------------------------------------------ //
// SERVER COFIG
// ------------------------------------------------------------------------------------------------------------ // 

var express = require('express');
var firebase = require('firebase');
var SerialPort = require('serialport');
var path = require('path');
const bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/static',express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views')); 
app.set('view engine', 'ejs');

var _get 		= {};
var fjordian 	= {};
var _PWD_ADM 	= "qweqwe";
var debug 		= true;


// ------------------------------------------------------------------------------------------------------------ //
// DATABASE / Google Firebase
// ------------------------------------------------------------------------------------------------------------ // 


// CONFIG VARIABLES
// --------------------------------

var config = {
    apiKey: "AIzaSyC0G286HCe2mlpOD5Vn2WgmzSm3ieBQWaQ",
    authDomain: "rfid-access-52370.firebaseapp.com",
    databaseURL: "https://rfid-access-52370.firebaseio.com",
    projectId: "rfid-access-52370",
    storageBucket: "",
    messagingSenderId: "908728539754"
  };

// DATABASE INIT + AUTH
// --------------------------------

firebase.initializeApp(config);
var database = firebase.database();
var sys_email = 'hugo@fjord.net';
var sys_pass = 'abcd5678';

firebase.auth().signInWithEmailAndPassword(sys_email, sys_pass).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  console.log("\n\nFirebase ERROR ---------------------------------");
  console.log(errorMessage);
  console.log("------------------------------------------------\n\n");
  // ...
});


// METHODS
// --------------------------------

function setFjordian(name, eid, coffee, coffeeCode, success, error) {

	database.ref('fjordians/' + coffeeCode).set({
		name: 		name,
		EID: 		eid,
		coffee: 	coffee,
		coffeeCode: coffeeCode,
		coffeeCount: 0
	})
	.then(success)
	.catch(error);


}

function getFjordian(coffeeCode, callback) {

	database.ref('fjordians/' + coffeeCode).on('value', function(data) {
			
			callback(data.val());

		});

}

function getCoffee(coffeeCode, success, error){

	database.ref('fjordians/' + coffeeCode).once('value')
	.then(function(data){
		
		var result = data.val();

		if(result != null){

			fjordian = data.val();

			var coffeeSet = {
					owner: 	fjordian.name,
					coffee: fjordian.coffee,
					EID: 	fjordian.EID
				}

			 success(coffeeSet);

		}
		else{
			error();
		}

	})
}

function setCoffee(coffeeCode,coffee){


	database.ref('fjordians/' + coffeeCode + '/history').push('value', function(data){

		if(debug){ console.log("Firebase [ Coffee Register = OK ]"); }

	}).set({
		datetime: "pegar data/hora do servidor",
		coffee: coffee,
		coffeeCode: coffeeCode
	});
	
		

}

function codeColor(color){
	switch(color){
		case 1: return 'preto'; break;
		case 2: return 'azul'; break;
		case 3: return 'verde'; break;
		case 4: return 'marrom'; break;
	}
}


// ------------------------------------------------------------------------------------------------------------ //
// SERIAL / MOTORS COMMANDS
// ------------------------------------------------------------------------------------------------------------ // 


// CONFIG VARIABLES
// --------------------------------
var serial1_path	= "/dev/ttyACM0";
var serial2_path 	= "/dev/ttyACM1";
var motors_status 	= [];


// MOTORS A - (1 and 2)
// --------------------------------

var motors_A = new SerialPort( serial1_path, { baudRate: 9600 });

motors_A.on('open',function(){
	console.log('[ SerialPort 1 | '+ serial1_path +' ] CONNECTED!');
});

motors_A.on('data',function(data){
	
	if(debug){
		console.log('\n\n');
		console.log('--------------------------------------------------------------------------');
		console.log('[ SerialPort 1 | '+ serial1_path +' ] DATA!');
		console.log('--------------------------------------------------------------------------');
		console.log(data);
		console.log('--------------------------------------------------------------------------');
		console.log('\n\n');
	}

});


// MOTORS B - (3 and 4)
// --------------------------------

var motors_B = new SerialPort( serial2_path, { baudRate: 9600 });

motors_B.on('open',function(){
	console.log('[ SerialPort 2 | '+ serial2_path +' ] CONNECTED!');
});

motors_B.on('data',function(data){
	
	if(debug){
		console.log('\n\n');
		console.log('--------------------------------------------------------------------------');
		console.log('[ SerialPort 2 | '+ serial2_path +' ] DATA!');
		console.log('--------------------------------------------------------------------------');
		console.log(data);
		console.log('--------------------------------------------------------------------------');
		console.log('\n\n');
	}

});


// MOTORS ACCESS METHODS
// --------------------------------

function runMotor(num){

	var speed_1 = 100;
	var speed_2 = 250;
	var delay 	= 500;
	var nLine 	= "\n";
	var motorsON  	= "EM,1,1" + nLine;
	var motorsOFF 	= "EM,0,0" + nLine;
	
	var cmd_motor_1  = "SM," + speed_1 + ",1600,0" + nLine;
		cmd_motor_1 += "SM," + delay + ",0,0" + nLine;
		cmd_motor_1 += "SM," + speed_2 + ",1600,0" + nLine;

	var cmd_motor_2  = "SM," + speed_1 + ",0,1600" + nLine;
		cmd_motor_2 += "SM," + delay + ",0,0" + nLine;
		cmd_motor_2 += "SM," + speed_2 + ",0,1600" + nLine;


	switch(num){

		case 1:
			console.log("M. 1");
			// motors_A.write(Buffer.from(motorsON + cmd_motor_1 + motorsOFF));
			break;

		case 2:
			console.log("M. 2");
			// motors_A.write(Buffer.from(motorsON + cmd_motor_2 + motorsOFF));
			break;

		case 3:
			console.log("M. 3");
			// motors_B.write(Buffer.from(motorsON + cmd_motor_1 + motorsOFF));
			break;

		case 4:
			console.log("M. 4");
			// motors_B.write(Buffer.from(motorsON + cmd_motor_2 + motorsOFF));
			break;
	}

}

function runCMD(script){

	var script = script.split(":");

	if(script[0] == ""){
		motors_A.write(Buffer.from(script[1]));
		motors_B.write(Buffer.from(script[1]));
	}
	if(script[0] == "A"){
		motors_A.write(Buffer.from(script[1]));
	}
	if(script[0] == "B"){
		motors_B.write(Buffer.from(script[1]));
	}

}

function checkMotors(){
	motors_A.write(Buffer.from("QM"));
	motors_B.write(Buffer.from("QM"));
}

// ------------------------------------------------------------------------------------------------------------ //
// MOTORS TEST
// ------------------------------------------------------------------------------------------------------------ // 

app.get('/mtest',function(req,res){

	res.render('mtest');
	
});

app.post('/mtest',function(req,res){

	var motor 		= req.body.motor;
	var cmd 		= req.body.cmd;

	if(debug){
	 console.log(motor + " - " + cmd);
	}

	if(!motor){
		runCMD(cmd);
	}
	else{
		runMotor(motor);
	}

	res.render('mtest');

	
});

// ------------------------------------------------------------------------------------------------------------ //
// APP  [Routes]
// ------------------------------------------------------------------------------------------------------------ // 

app.get('/app',function(req,res){

	res.render('app');
	
});

app.post('/app',function(req,res){

	var coffee 		= req.body.coffee;
	var coffeeCode 	= req.body.coffeeCode;

	if(coffeeCode.length < 3){
		res.render('app');
	}
	else{

		getCoffee(coffeeCode, 
			function(data){
				
				if(coffee == 'undefined'){
					coffee = data.coffee;
				}

				console.log(coffee);
				//runMotor(coffee);
				//setCoffee(coffeeCode,coffee);

				res.render('app-result', {title:"Aproveite seu café", type:"success"});
			},
			function(){
				res.render('app-result', {title:"Não autorizado, cai fora !!!", type:"error"});
			});

	}
	
});

// ------------------------------------------------------------------------------------------------------------ //
// ADM  [Routes]
// ------------------------------------------------------------------------------------------------------------ // 

app.get('/', function(req, res){
	_get = req.query;
	res.render('index',{title:"IoT Coffe"});

});

app.get('/register',function(req,res){ res.redirect('/'); });
app.post('/register',function(req,res){

	if(req.body.pwd_input == _PWD_ADM){
		res.render('register');
	}
	else{
		res.redirect('/');
	}


});

app.get('/result',function(req,res){ res.redirect('/'); });
app.post('/result',function(req,res){

	setFjordian(req.body.name, req.body.eid, req.body.coffee, req.body.coffeeCode, 
		function(){

			var coffeeColor = '';
			switch(req.body.coffee){
				case "1": coffeeColor = 'preto'; break;
				case "2": coffeeColor = 'azul'; break;
				case "3": coffeeColor = 'verde'; break;
				case "4": coffeeColor = 'marrom'; break;
			}

			res.render('result',{
				type: 		'success',
				title: 		'Fjordian cadastrado com sucesso!',
			  	coffeeCode: req.body.coffeeCode,
			  	name: 		req.body.name,
			  	eid: 		req.body.eid,
			  	coffee: 	coffeeColor
			  });
		}, 
		function(){

			res.render('result',{
				type: 		'error',
				title: 		'Ocorreu algum erro ao cadastrar. Tente novamente mais tarde!',
			  	coffeeCode: '--------------------',
			  	name: 		'--------------------',
			  	eid: 		'--------------------',
			  	coffee: 	'--------------------'
			  });
		});


});


// ------------------------------------------------------------------------------------------------------------ //
// SERVER START
// ------------------------------------------------------------------------------------------------------------ // 

app.listen(3000);
